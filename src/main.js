import {SoundSystem} from './soundSystem.js';
import {Game} from './Game.js';

$(document).ready(function () {
    let game = new Game(pressPad, releasePad, updateLevelView, gameWon);
    let soundSystem = new SoundSystem();

    // UI Functionality --------------------------------------------------------
    let color_pad = document.getElementById("gamepad-map");
    color_pad.addEventListener("mousedown", (e) => {
        if (game.isPlayerTurn) {
            pressPad(e.target.id);
        }
    });

    color_pad.addEventListener("mouseup", (e) => {
        if (game.isPlayerTurn) {
            releasePad();
            let color = Number(e.target.id);
            game.input_step(color);
            //console.log("input: " + game.input_sequence);
        }
    });

    document.getElementById("start-game").addEventListener("click", (e) => {
        if (game.status == "stopped") {
            //console.log("Game on!");
            game.status = "in progress";
            game.show_solution();
            $("#start-game").text("Reset Game");
        } else if (game.status == "in progress") {
            game.reset();
            $("#start-game").text("Start Game");
            let pic = document.getElementById("gamepad");
            pic.src = 'img/gamepad-off.png'
            //console.log("game reset");
        }
    });

    function pressPad(area) {
        // Update HTML for when the user presses the pad.
        // Also used as a callback function for when the Game is showing the user the sequence, 
        // and the game pad is pressed. 
        let pic = document.getElementById("gamepad");
        pic.src = 'img/gamepad-'+ area +'.png'
        soundSystem.play(area);
    }

    function releasePad() {
        // Update HTML for when the user releases the pad.
        // Also used as a callback function for when the Game is showing the user the sequence, 
        // and the game pad is released.
        let pic = document.getElementById("gamepad");
        pic.src = 'img/gamepad-off.png'
    }

    document.getElementById("btn-strict").addEventListener("click", (e) => {
        if (game.status !== "stopped") {
            return;
        }

        game.inStrictMode = !(game.inStrictMode);
        //console.log("game inStrictMode:" + game.inStrictMode);
        
        if (game.inStrictMode) {
            e.target.src = "img/strict-on.png";
        } else {
            e.target.src = "img/strict-off.png";
        }

    });

    function updateLevelView(level) {
        // Callback function to show the current level.
        document.getElementById("level").innerHTML = level;
    }
    
    function gameWon() {
        // Callback function when the user wins.
        $("#start-game").text("Start Game");
    }

});
