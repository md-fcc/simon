export class Game {
    
    constructor(cbPushPad, cbReleasePad, cbUpdateLevelView, cbWin) {
        this.input_sequence = []; // stores the sequence of colors the player has input
        this.status = "stopped";
        this.isPlayerTurn = false;
        this.level = 1;
        this.max_levels = 20;
        this.reactionTimerID = null;
        this.inputTimerID = null;
        this.showTimerID = null;
        this.inStrictMode = false; // strict mode starts a new game on 1st mistake
        this.solution = [];
        this.build_solution();

        // Callbacks
        this.pushPad = cbPushPad;
        this.releasePad = cbReleasePad;
        this.levelView = cbUpdateLevelView;
        this.notifyWin = cbWin;
    }
    
    build_solution() {
        // populates the color for each level of the game.
        this.solution = [];
        for (let i = 0; i < this.max_levels; i++) {
            let color = Math.floor((Math.random() * 4) + 1);
            this.solution.push(color);
        }
        //console.log(this.solution);
    }
    
    check_answer() {
        // Handles the logic at the end of the player's turn to determine if 
        // they can advance to the next level, or if the game is over.

        if (this.sequences_match()) {
            //console.log('input matches answer key!');
            if (this.level == this.max_levels) {
                //console.log("You Win!");
                alert("You Win!");
                this.reset();
                this.notifyWin();
            } else {
                this.level++;
                this.levelView(this.level);
                this.show_solution(); 
            }
        } else {
            //console.log('input does not match answer key :(');
            if (this.inStrictMode) {
                this.build_solution();
                this.level = 1;
                this.levelView(this.level);
                this.show_solution();
            } else {
                // show the same sequence again
                this.show_solution(); 
            }
        }
    }

    sequences_match() {
        // Simple array comparison of the this.input_sequence and the game
        // solution sequence up to, and including, the current level.
        let current_solution = this.solution.slice(0, this.level);

        if (this.input_sequence == null || current_solution == null) return false;
        if (this.input_sequence.length != current_solution.length) return false;
        
        for (let i = 0, len = current_solution.length; i < len; ++i) {
            if (current_solution[i] !== this.input_sequence[i]) return false;
        }
        
        return true;
    }

    show_solution() {
        // The computer will show each color in the current sequence so that
        // the player can learn what to input when it's their turn.

        this.isPlayerTurn = false;
        let current_seq = this.solution.slice(0, this.level);
        //console.log(current_seq);

        let showLevel = 0;
        this.pushPad(current_seq[showLevel]);
        showLevel++;
        let nextPress = false;

        this.showTimerID = setInterval( () => {
            if (nextPress) {
                this.pushPad(current_seq[showLevel]);
                //console.log("showTimer:" + current_seq[showLevel]);
                showLevel++;
                nextPress = false;              
            } else {
                this.releasePad();
                nextPress = true;
                if (showLevel == this.level) {
                    //clearInterval(releseTimerID);
                    clearInterval(this.showTimerID);
                    this.wait_for_player_input();
                }   
            }
        }, 250);
    }

    wait_for_player_input() {
        // When it's the players turn, the computer will wait for their input,
        // but expects their first input within reaction_time miliseconds.

        let reaction_time = 3000;
        this.isPlayerTurn = true;
        //console.log("Player's turn...");
        // expect player to respond within 3000 ms
        this.reactionTimerID = setTimeout(
            () => {
                console.log("no player input...");
                this.reactionTimerID = null;
                this.check_answer();
                this.clear_input();
            }, reaction_time);
    }

    input_step(step) {
        // Handles player input. After end_turn_delay milliseconds without input,
        // the player's turn is assumed to be over.

        let end_turn_delay = 2000;
        clearTimeout(this.reactionTimerID);
        this.reactionTimerID = null;

        this.input_sequence.push(step);
        
        // Clear any existing timer, start a new timer
        if (this.inputTimerID) {
            clearTimeout(this.inputTimerID);
            this.inputTimerID = null;
        }

        this.inputTimerID = setTimeout(
            () => {
                this.inputTimerID = null;
                this.isPlayerTurn = false;
                this.check_answer();
                this.clear_input();
            }, end_turn_delay);
    }

    clear_input() {
        // deletes the player's input sequence
        this.input_sequence = [];
    }

    reset() {
        // resets the game to the defaults (except for user prefs like strict mode)
        this.clear_input();
        this.level = 1;
        this.levelView(this.level);
        this.build_solution();
        clearTimeout(this.inputTimerID);
        clearTimeout(this.reactionTimerID);
        clearTimeout(this.showTimerID);
        this.inputTimerID = null;
        this.reactionTimerID = null;
        this.showTimerID = null;
        this.status = "stopped";
    }
}
