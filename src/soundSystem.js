class SoundEffect {
    // https://www.w3schools.com/graphics/game_sound.asp
    constructor(src) {
        this.sound = document.createElement("audio");
        this.sound.src = src;
        this.sound.setAttribute("preload", "auto");
        this.sound.setAttribute("controls", "none");
        this.sound.style.display = "none";
    }

    play() {this.sound.play();}
    stop() {this.sound.pause();}
    load() {this.sound.load();}
}

export class SoundSystem {
    constructor() {
        this.effects = [];
        this.effects[1] = new SoundEffect('https://s3.amazonaws.com/freecodecamp/simonSound1.mp3');
        this.effects[2] = new SoundEffect('https://s3.amazonaws.com/freecodecamp/simonSound2.mp3');
        this.effects[3] = new SoundEffect('https://s3.amazonaws.com/freecodecamp/simonSound3.mp3');
        this.effects[4] = new SoundEffect('https://s3.amazonaws.com/freecodecamp/simonSound4.mp3');
    }

    play(effect) {
        if (effect < 1 || effect > this.effects.length) {
            console.log('unknown sound');
            return;
        }

        if (window.chrome) {
            this.effects[effect].load();
        }
        this.effects[effect].play();
    }
}

