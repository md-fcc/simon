My Simon game clone to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-simon-game).

Live website: [Simon](https://md-fcc.gitlab.io/simon/)

The Javascript source code is split into three modules:
- main.js - Entry point to the code. instantiates the Game and assigns functionality to the game buttons.
- Game.js - Contains the Game class and logic for Simon. When the Game is instantiated, a random sequence of 20 colors is generated as the key "solution" for beating the game. User input is compared to the the solution after each click on a game pad. The Game class uses callbacks to the main.js functions for updating the HTML.
- soundSystem.js - Holds the logic for playing sounds.

I learned about Webpack and Babel tools while building this game. Not a lot of browsers support the ES6 module import/export features, so I am using these tools to transpile the code.
